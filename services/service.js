'use strict';

const customerModel = require('../models/customerModel');
const orderModel = require('../models/ordersModel');
const to = require('await-to-js').default;
const services = module.exports;

services.insertCustmerData = async (apiRequest, apiResponse) => {
    let fullName = 'Eshwar'
    let email = 'eshwar@gmail.com'

    if (!fullName) {
        apiResponse.send({ message: 'Name is required.', status: 'ERROR' });
    } else if (!email) {
        apiResponse.send({ message: 'Email is required.', status: 'ERROR' });
    } else {
        const custData = new customerModel();
        custData['fullName'] = fullName
        custData['email'] = email
        custData.save((errSave, custDataUpdated) => {
            if (errSave) {
                apiResponse.send({ message: 'Error while saving customer profile', status: 'ERROR', error: errSave });
            } else {
                apiResponse.send({ message: 'customer profile saved successfully', data: {}, status: 'SUCCESS' });

            }
        });
    }
}

services.insertOrderData = async (apiRequest, apiResponse) => {
    let orderId = 20
    let custId = 1

    if (!orderId) {
        apiResponse.send({ message: 'orderId is required.', status: 'ERROR' });
    } else if (!custId) {
        apiResponse.send({ message: 'custId is required.', status: 'ERROR' });
    } else {
        const orderData = new orderModel();
        orderData['orderId'] = orderId
        orderData['custId'] = custId
        orderData['status'] = 1
        orderData['orderDate'] = Date.now()
        orderData.save((errSave, custDataUpdated) => {
            if (errSave) {
                apiResponse.send({ message: 'Error while saving order details', status: 'ERROR', error: errSave });
            } else {
                apiResponse.send({ message: 'order details saved successfully', data: {}, status: 'SUCCESS' });

            }
        });
    }
}

services.AllUsers = async function (apireq, apires) {

    const [findError, findData] = await to(customerModel.find());
    // UsersModel.findOne(query, (findError, findData) => {
    if (findError) {
        return apires.send({
            status: 'ERROR',
            message: 'something went wrong!!'
        });
    }
     if (findData) {
            return apires.send({
                status: 'SUCCESS',
                message: 'customers data fetched successfully!',
                data: findData
            });
     } else{
        return apires.send({
            status: 'FAILED',
            message: 'No data Found',
        });
     }
};



services.Allorders = async function (apireq, apires) {
    let custId = 1    // apireq.body.custId
     // if it is post method
    const [findError, findData] = await to(orderModel.find({custId:1}));
    if (findError) {
        return apires.send({
            status: 'ERROR',
            message: 'something went wrong!!'
        });
    }
     if (findData) {
            return apires.send({
                status: 'SUCCESS',
                message: 'customers data fetched successfully!',
                data: findData
            });
     } else{
        return apires.send({
            status: 'FAILED',
            message: 'No data Found',
        });
     }
};


services.RelationData = async function (apireq, apires) {
    let custId = 1    // apireq.body.custId
     // if it is post method
    const [findError, findData] = await to(orderModel.aggregate([
        {
          $lookup:
            {
              from: "customers",
              localField: "custId",
              foreignField: "_id",
              as: "relational_docs"
            }
       }
     ]))
    if (findError) {
        return apires.send({
            status: 'ERROR',
            message: 'something went wrong!!'
        });
    }
     if (findData) {
            return apires.send({
                status: 'SUCCESS',
                message: 'customers data fetched successfully!',
                data: findData
            });
     } else{
        return apires.send({
            status: 'FAILED',
            message: 'No data Found',
        });
     }
};