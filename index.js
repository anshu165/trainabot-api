const express = require('express')
const app = express();
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const config = require('config');

const port = 9090;
const options = {
  autoReconnect: true,
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 1000, // Reconnect every 1 s
  poolSize: 25, // Maintain up to 25 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  keepAlive: 120,
  promiseLibrary: mongoose.Promise,
  useNewUrlParser: true,
  useCreateIndex: true
};

const CustmerDetails = require('./routes/routes');
const db = mongoose.connection;

db.once('error', (err) => {
  // console.error(`mongoose connection error${err}`);
  mongoose.disconnect();
});
db.on('open', () => {
  // console.log('successfully connected to mongoose');
});
db.on('reconnected', () => {
  // console.log('MongoDB reconnected!');
});
db.on('disconnected', () => {
  // console.log('MongoDB disconnected!');
  // if (!processExit) {
  // mongoose.connect(config.mongodb.uri, options);
  // .then(() =>
  // console.log('connection successful'))
  // .catch(err => console.error(err));
  // }
});

// mongoose.connect(config.mongodb.uri, options);

mongoose.connect(config.mongodb.uri, options);


app.get('/', (req, res) => {
  res.send('Hello World!')
});
app.use('/all', CustmerDetails);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`)
});
