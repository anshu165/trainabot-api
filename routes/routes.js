'use strict';

const express = require('express');
const custServices = require('../services/service');



const router = express.Router();

router.get('/', custServices.insertCustmerData)
router.get('/order/', custServices.insertOrderData)
router.get('/allUsers/', custServices.AllUsers)
router.get('/allOrders/', custServices.Allorders)
router.get('/relation/', custServices.RelationData)

module.exports = router;
