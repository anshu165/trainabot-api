'use strict';

const mongoose = require('mongoose');

const { Schema } = mongoose;
const AutoIncrement = require('mongoose-sequence')(mongoose);
const CustomerModel = new Schema({
  _id : Number,
  email: {
    type: String,
    required: true
  },
  fullName: {
    type: String,
    required: true
  }
},{ _id: false, versionKey: false });
CustomerModel.plugin(AutoIncrement);


module.exports = mongoose.model('customers', CustomerModel);
