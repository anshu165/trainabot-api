'use strict';

const mongoose = require('mongoose');

const { Schema } = mongoose;

const orderModel = new Schema({
  orderId: {
    type: Number,
    required: true
  },
  status: {
    type: Number
  },
  custId: {
    type: Number,
    required: true
  },
  orderDate:{
    type: Number,
    default: Date.now()
  }
},{ versionKey: false });

orderModel.index({
    orderId: 1,
    custId: 1
  }, {
    name: 'orders_table_index',
    background: true
  });
module.exports = mongoose.model('orders', orderModel);